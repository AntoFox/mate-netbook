mate-netbook (1.24.0-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control:
    + Add Rules-Requires-Root: field and set it to no.
    + Bump Standards-Version: to 4.5.0. No changes needed.
  * debian/upstream/metadata:
    + Drop obsolete fields Contact: and Name:.
    + Append .git suffix to URLs in Repository: field.

  [ Martin Wimpress ]
  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 14 Feb 2020 16:40:45 +0100

mate-netbook (1.22.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/patches:
    + Drop 0001_features_and_fixes.patch. Applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 28 Sep 2019 00:43:06 +0200

mate-netbook (1.22.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/patches:
    + Drop 0001_fix_seg_fault.patch. Applied upstream.
    + Add 0001_features_and_fixes.patch.

  [ Mike Gabriel ]
  * debian/{compat,control}:
    + Use debhelper-compat notation. Bump to DH compat level version 12.
  * debian/control:
    + Bump Standards-Version: to 4.4.0. No changes needed.
    + Remove empty line at EOF.
  * debian/rules:
    + Drop dbgsym-migration dh_strip override.
  * debian/copyright:
    + Update copyright attributions.
  * debian/patches:
    + Add patch header to 0001_features_and_fixes.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 31 Jul 2019 16:07:29 +0200

mate-netbook (1.20.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Update Vcs-*: fields. Package has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + Drop pkg-mate-team Alioth mailing list from Uploaders: field.
  * debian/copyright:
    + Use secure URI for copyright format.
    + Make Upstream-Name: field's value more human readable.
    + Update Upstream-Contact: field.
    + Update Source: field. Use secure URLs.
    + Update copyright attributions.
  * debian/upstream/metadata:
    + Add file. Be compliant with DEP-12 proposal.
    + Use Martin's address as Contact: address.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 05 Jul 2018 17:16:16 +0200

mate-netbook (1.20.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/copyright (already in 1.19.0-0ubuntu1):
    + Update copyright attributions.

  [ Vangelis Mouhtsis ]
  * debian/control (already in 1.19.0-0ubuntu1):
    + Temporarily have pkg-mate-team ML under Uploaders:.
    + Update Maintainer: field to debian-mate ML on lists.debian.org.
    + Rename pretty name of our team -> Debian+Ubuntu MATE Packaging Team.
    + Bump Standards-Version: to 4.1.3. No changes needed.

  [ Mike Gabriel ]
  * debian/{control,compat}: Bump DH version level to 11.
  * debian/watch:
    + Use secure URL to obtain upstream sources.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 14 Feb 2018 14:21:20 +0100

mate-netbook (1.20.0-0ubuntu1) bionic; urgency=medium

  * New upstream release.

 -- Martin Wimpress <martin.wimpress@ubuntu.com>  Wed, 07 Feb 2018 14:03:57 +0000

mate-netbook (1.19.0-0ubuntu1) bionic; urgency=medium

  * New upstream release.

 -- Martin Wimpress <martin.wimpress@ubuntu.com>  Thu, 25 Jan 2018 21:45:06 +0000

mate-netbook (1.18.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.1.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 02 Nov 2017 10:02:25 +0100

mate-netbook (1.18.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/control: Dependencies updated for MATE 1.17.
  * debian/rules: Remove obsolete --with-gtk=3.0
  * debian/control: Dependencies updated for MATE 1.18.

  [ Aron Xu ]
  * New upstream release.

  [ Vangelis Mouhtsis ]
  * debian/compat:
    + Update compat version.
  * debian/control:
    + Bump debhelper version to (>= 10.3~).
  * debian/control:
    + Bump Standards-Version: to 4.0.0. No changes needed.
  * debian/rules:
    + Add new block for override_dh_missing.
  * debian/rules:
    + Disable autoreconf, handled by autogen.sh.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 25 Jul 2017 18:57:49 +0200

mate-netbook (1.16.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream bug fix release.
    - Fix broken mate-window-picker-applet. (LP: #1608710).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 12 Oct 2016 08:30:40 +0200

mate-netbook (1.16.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Drop B-D: libunique-3.0-dev. Not required by this upstream version
      anymore. (Closes: #827229). (Already fixed in 1.15 upload).

  [ Vangelis Mouhtsis ]
  * debian/changelog:
    + Fix missing changelog items for 1.15.2-0ubuntu1 upload.
  * debian/control:
    + Dependencies updated for MATE 1.16.
  * debian/watch:
    + Update version matching again to _not_ support development releases.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 04 Oct 2016 10:24:29 +0200

mate-netbook (1.15.2-0ubuntu1) yakkety; urgency=medium

  [ Martin Wimpress ]
  * New upstream development release. (Closes: #827229).
  * debian/control:
    + Dependencies updated for MATE 1.15.
  * debian/watch:
    + Update version matching to support development releases.

  [ Vangelis Mouhtsis ]
  * debian/{control,rules}:
    + dbgsym: Don't build dbg:packages anymore.
      See https://wiki.debian.org/AutomaticDebugPackages.

 -- Martin Wimpress <code@flexion.org>  Fri, 09 Sep 2016 13:04:48 +0100

mate-netbook (1.14.0-1) unstable; urgency=medium

  [ Vangelis Mouhtsis ]
  * debian/control:
    + Versioned B-D on dpkg-dev (>= 1.16.1.1).
    + Use encrypted URLs for Vcs-*: field.
  * debian/rules:
    + Enable all hardening flags.

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + B-D: libgtk-3-dev
    + Versioned B-D: libmate-desktop-dev (>= 1.14)
    + Versioned B-D: libmate-panel-applet-dev (>= 1.14)
    + B-D: libunique-3.0-dev
    + B-D: libwnck-3-dev.
    + Versioned B-D: mate-common (>= 1.14)
  * debian/rules:
    + MateConf dropped upstream.
    + Add --with-gtk=3.0
  * debian/copyright:
    + Update copyright attributions.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards: to 3.9.8. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 26 May 2016 05:18:34 +0200

mate-netbook (1.12.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Versioned B-D: mate-common (>= 1.12), libmate-panel-applet-dev
      (>= 1.12) and libmate-desktop-dev (>= 1.12).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 02 Jan 2016 11:59:31 +0100

mate-netbook (1.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Add mate-netbook-common package.
  * debian/patches:
    + Drop 0001_preserve_configuration.patch. Applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 21 Aug 2015 16:48:59 +0200

mate-netbook (1.8.2-2) unstable; urgency=medium

  [ Martin Wimpress ]
  * Upload to unstable.
  * debian/patches:
    + Add 0002_preserve_configuration.patch. Ensure Window Picker
      applet doesn't override mate-maximus. (Closes: #785090).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 19 May 2015 17:29:09 +0200

mate-netbook (1.8.2-1) experimental; urgency=medium

  * New upstream release.
    - Including GLib >= 2.43 compatibility patch. (Closes: #779829,
      LP:#1426327).
  * debian/patches:
    + Drop 0001_respect_undecorate_setting.patch. Included in lates upstream
      release.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 13 Mar 2015 14:09:37 +0100

mate-netbook (1.8.1-4) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/patches:
    + Add 0001_respect_undecorate_setting.patch. Ensure mate-maximus
      undecorates maximized windows only when the "undecorate" dconf
      option is set. (Closes: #778816).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 05 Mar 2015 08:35:48 +0100

mate-netbook (1.8.1-3) unstable; urgency=medium

  * Follow-up fix for #769417: Install mate-window-picker-applet to
    /usr/lib/mate-netbook, not directly into /usr/lib.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 12 Dec 2014 10:20:42 +0100

mate-netbook (1.8.1-2) unstable; urgency=medium

  * Install mate-window-picker-applet to non-multi-arch libexecdir. (Closes:
    #769417).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 12 Dec 2014 09:33:52 +0100

mate-netbook (1.8.1-1) unstable; urgency=medium

  [ Vangelis Mouhtsis ]
  * debian/control:
    + Bump Standards: to 3.9.6. No changes needed.

  [ Mike Gabriel ]
  * New upstream release.
  * debian/rules:
    + Use upstream's NEWS file as upstream ChangeLog.
  * debian/man:
    + Drop man page for mate-maximus. Now provided by upstream.
  * debian/patches:
    + Drop 0001_fix-mwp-applet-segfaulting-while-loading.patch.
      Applied upstream.
  * debian/copyright:
    + Really mention all upstream source tree files.
  * debian/mate-netbook.install:
    + Install mate-maximus.1 man page from DESTDIR.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 23 Oct 2014 09:28:34 +0200

mate-netbook (1.8.0-2) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/mate-netbook.install:
    + Drop mate-conf .convert files from package (again).
  * debian/patches:
    + Add 0001_fix-mwp-applet-segfaulting-while-loading.patch.
      Fix segfaulting of mate-windowpicker-applet while it is loading. (Closes:
      #748351).

  [ Vangelis Mouhtsis ]
  * debian/rules:
    + Replace on dh_install --list-missing -> --fail-missing.
    + Remove non-packaged *.convert files after build.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Jul 2014 23:14:23 +0200

mate-netbook (1.8.0-1) unstable; urgency=low

  * Initial release. (Closes: #734940).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 25 Apr 2014 22:34:18 +0200
